package com.shantanupatil.youtubeplaylist.database;

import com.shantanupatil.youtubeplaylist.R;

public class MoreAppsData {
    private int[] icons = {
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background,
            R.drawable.ic_launcher_background
    };

    private String[] name = {
            "App 1",
            "App 2",
            "App 3",
            "App 4",
            "App 5"
    };

    private String[] urls = {
            "https://url1.com",
            "url 2",
            "url 3",
            "url 4",
            "url 5"
    };

    public int getIcon(int position) {
        return icons[position];
    }

    public String getName(int position) {
        return name[position];
    }

    public String getUrl(int position) {
        return urls[position];
    }

    public int getSize() {
        return name.length;
    }
}
