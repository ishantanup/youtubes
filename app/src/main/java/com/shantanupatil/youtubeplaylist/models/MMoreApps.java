package com.shantanupatil.youtubeplaylist.models;

public class MMoreApps {
    private int drawable;
    private String nameOfApp;
    private String urlOfApplication;

    public MMoreApps(int drawable, String nameOfApp, String urlOfApplication) {
        this.drawable = drawable;
        this.nameOfApp = nameOfApp;
        this.urlOfApplication = urlOfApplication;
    }

    public String getNameOfApp() {
        return nameOfApp;
    }

    public String getUrlOfApplication() {
        return urlOfApplication;
    }

    public int getDrawable() {
        return drawable;
    }
}