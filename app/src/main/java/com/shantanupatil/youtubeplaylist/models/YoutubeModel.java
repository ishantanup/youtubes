package com.shantanupatil.youtubeplaylist.models;

public class YoutubeModel {
    private String title;
    private String publishedAt;
    private String thumbnail;
    private String description;
    private String videoID;

    public YoutubeModel(String title, String publishedAt, String thumbnail, String description, String videoID) {
        this.title = title;
        this.publishedAt = publishedAt;
        this.thumbnail = thumbnail;
        this.description = description;
        this.videoID = videoID;
    }

    public String getVideoID() {
        return videoID;
    }

    public String getTitle() {
        return title;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getDescription() {
        return description;
    }
}
