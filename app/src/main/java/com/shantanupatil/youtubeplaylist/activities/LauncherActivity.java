package com.shantanupatil.youtubeplaylist.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.messaging.FirebaseMessaging;
import com.shantanupatil.youtubeplaylist.Config;
import com.shantanupatil.youtubeplaylist.R;
import com.shantanupatil.youtubeplaylist.activities.HomeActivity;

public class LauncherActivity extends AppCompatActivity {

    //After 2000 ms the HomeActivity will launch
    private final int TIME_OUT = 3000;

    //Progressbar
    ProgressBar launcherProgress;

    private InterstitialAd mInterstitialAd;
    private Config config;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);

        config = new Config();

        //initialize progressbar
        launcherProgress = (ProgressBar) findViewById(R.id.launcher_progress_bar);

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(config.INTERSTITIAL);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mInterstitialAd.loadAd(adRequest);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mInterstitialAd.setAdListener(new AdListener() {
                    @Override
                    public void onAdLoaded() {
                        super.onAdLoaded();
                        mInterstitialAd.show();
                    }

                    @Override
                    public void onAdClosed() {
                        super.onAdClosed();
                    }

                    @Override
                    public void onAdFailedToLoad(int i) {

                    }
                });
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        }, TIME_OUT);
    }
}
