package com.shantanupatil.youtubeplaylist.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.shantanupatil.youtubeplaylist.R;
import com.shantanupatil.youtubeplaylist.adapter.MoreAppsAdapter;
import com.shantanupatil.youtubeplaylist.controller.MoreAppsController;
import com.shantanupatil.youtubeplaylist.database.MoreAppsData;
import com.shantanupatil.youtubeplaylist.interfaces.IPassDataMoreApps;
import com.shantanupatil.youtubeplaylist.models.MMoreApps;

import java.util.ArrayList;

public class MoreApps extends AppCompatActivity implements IPassDataMoreApps {

    private ArrayList<MMoreApps> mMoreApps;
    private MoreAppsData moreAppsData;

    private RecyclerView recyclerView;
    private MoreAppsAdapter moreAppsAdapter;
    private MoreAppsController controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_apps);

        mMoreApps = new ArrayList<>();
        moreAppsData = new MoreAppsData();

        controller = new MoreAppsController(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_more_apps);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("More Apps");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitleTextColor(Color.WHITE);

        for (int i = 0; i < moreAppsData.getSize(); i++) {
            MMoreApps mMoreApp = new MMoreApps(
                    moreAppsData.getIcon(i),
                    moreAppsData.getName(i),
                    moreAppsData.getUrl(i)
            );
            mMoreApps.add(mMoreApp);
        }

        initRecyclerView();


        moreAppsAdapter = new MoreAppsAdapter(mMoreApps, getApplicationContext(), controller);
        recyclerView.setAdapter(moreAppsAdapter);
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_more_apps);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

    }

    @Override
    public void passData(String url) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong, " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
