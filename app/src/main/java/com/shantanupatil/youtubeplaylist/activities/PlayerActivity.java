package com.shantanupatil.youtubeplaylist.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.google.android.youtube.player.YouTubePlayerView;

import com.shantanupatil.youtubeplaylist.Config;
import com.shantanupatil.youtubeplaylist.R;

public class PlayerActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    Config config;
    YouTubePlayerFragment youTubePlayerFragment;
    String videoID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        config = new Config();
        Intent intent = getIntent();
        videoID = intent.getStringExtra("videoId");

        try {
            youTubePlayerFragment = (YouTubePlayerFragment) getFragmentManager().findFragmentById(R.id.youtube_player_fragment);
            youTubePlayerFragment.initialize(config.API_KEY, this);
        } catch (IllegalStateException s) {
            Log.d("PlayerActivity", "onCreate: " + s.getMessage());
        }

    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo(videoID);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            try {
                getYouTubePlayerVideo().initialize(config.API_KEY, this);
            } catch (Exception e) {

            }
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }

    public YouTubePlayer.Provider getYouTubePlayerVideo() {
        return (YouTubePlayerView) findViewById(R.id.youtube_player_fragment) ;
    }
}
