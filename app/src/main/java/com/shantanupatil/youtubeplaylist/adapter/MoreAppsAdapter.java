package com.shantanupatil.youtubeplaylist.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shantanupatil.youtubeplaylist.R;
import com.shantanupatil.youtubeplaylist.controller.MoreAppsController;
import com.shantanupatil.youtubeplaylist.models.MMoreApps;

import java.util.ArrayList;

public class MoreAppsAdapter extends RecyclerView.Adapter<MoreAppsAdapter.ViewHolder> {
    ArrayList<MMoreApps> mMoreApps;
    Context context;
    MoreAppsController controller;

    public MoreAppsAdapter(ArrayList<MMoreApps> mMoreApps, Context context, MoreAppsController controller) {
        this.mMoreApps = mMoreApps;
        this.context = context;
        this.controller = controller;
    }

    @NonNull
    @Override
    public MoreAppsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.more_apps_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MoreAppsAdapter.ViewHolder holder, int position) {
        MMoreApps mMoreApp = mMoreApps.get(position);
        holder.nameOfApp.setText(mMoreApp.getNameOfApp());
        holder.nameOfApp.setTypeface(Typeface.createFromAsset(context.getAssets(), "circularstd.ttf"));
        holder.nameOfApp.setTextColor(Color.BLACK);

        Glide.with(context).load(mMoreApp.getDrawable()).into(holder.imgText);

    }

    @Override
    public int getItemCount() {
        return mMoreApps.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView imgText;
        TextView nameOfApp;
        LinearLayout containerLayout;
        Button openApp;

        public ViewHolder(View itemView) {
            super(itemView);

            imgText = (ImageView) itemView.findViewById(R.id.img_init);
            nameOfApp = (TextView) itemView.findViewById(R.id.name_apps_more);
            containerLayout = (LinearLayout) itemView.findViewById(R.id.more_apps_container);
            openApp = (Button) itemView.findViewById(R.id.install_the_app);

            openApp.setOnClickListener(this);
            containerLayout.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            MMoreApps mMoreApp = mMoreApps.get(getAdapterPosition());
            controller.onItemClick(mMoreApp.getUrlOfApplication());
        }
    }
}
