package com.shantanupatil.youtubeplaylist.adapter;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.lang.reflect.Type;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.shantanupatil.youtubeplaylist.R;
import com.shantanupatil.youtubeplaylist.controller.Controller;
import com.shantanupatil.youtubeplaylist.database.SQLHelper;
import com.shantanupatil.youtubeplaylist.models.YoutubeModel;

import org.w3c.dom.Text;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class YoutubeAdapter extends RecyclerView.Adapter<YoutubeAdapter.ViewHolder>{

    private static final String TAG = "YoutubeAdapterDebugging";
    private List<YoutubeModel> youtubeModelList;
    private Context context;
    private final int HEAD = 0;
    private final int LIST = 5;
    private final int AD = 2;
    private final int APP_INSTALL = 3;
    Controller controller;

    public YoutubeAdapter(List<YoutubeModel> youtubeModelList, Context context, Controller controller) {
        this.youtubeModelList = youtubeModelList;
        this.context = context;
        this.controller = controller;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        ViewHolder viewHolder;
        if (viewType == LIST) {
            Log.d(TAG, "onCreateViewHolder: " +LIST);
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.youtubelist_item, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        } else if (viewType == HEAD) {
            Log.d(TAG, "onCreateViewHolder: " +HEAD);

            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.youtubelist_item_head, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        } else if (viewType == AD) {
            Log.d(TAG, "onCreateViewHolder: " +AD);

            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.adview, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        }  else if (viewType == APP_INSTALL) {
            Log.d(TAG, "onCreateViewHolder: " +APP_INSTALL);

            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.app_install, parent, false);
            viewHolder = new ViewHolder(view, viewType);
            return viewHolder;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final YoutubeModel youtubeModel = youtubeModelList.get(position);
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "circularstd.ttf");
        final SQLHelper sqlHelper = new SQLHelper(context);
        final int[] flag = {1};

        if (holder.view_type == LIST) {
            Log.d(TAG, "OnBindView: " + LIST);
            holder.listTitle.setText(youtubeModel.getTitle());
            holder.publishedAt.setText(getPublishAtDate(youtubeModel.getPublishedAt()));
            holder.description.setText(youtubeModel.getDescription());
            holder.listTitle.setTypeface(typeface);
            holder.listTitle.setTextColor(Color.BLACK);
            Glide.with(context)
                    .load(youtubeModel.getThumbnail())
                    .apply(new RequestOptions()
                    .placeholder(R.drawable.circle_placeholder))
                    .into(holder.listThumb);

            holder.favourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Cursor cursor = sqlHelper.getAllData();
                    while (cursor.moveToNext()) {
                        String temp_id = cursor.getString(2);
                        if (temp_id.equals(youtubeModel.getVideoID())) {
                            flag[0] = 0;
                        }
                    }
                    if (flag[0] != 0) {
                        boolean res = sqlHelper.insertData(youtubeModel.getTitle(), youtubeModel.getVideoID(), youtubeModel.getThumbnail(), youtubeModel.getDescription(), youtubeModel.getPublishedAt());
                        if (res) {
                            //holder.favourite.setImageResource(R.drawable.yesfavourite);
                            Toast.makeText(context, "Added to favourite", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(context, "Already Favourite", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else if (holder.view_type == HEAD) {
            Log.d(TAG, "OnBindView" + HEAD);
            holder.headTitle.setText(youtubeModel.getTitle());
            holder.headTitle.setTypeface(typeface);
            Glide.with(context).load(youtubeModel.getThumbnail()).into(holder.headThumb);
        } else if (holder.view_type == AD) {
            Log.d(TAG, "OnBindView" + AD);
            //setting the banner ad
            AdRequest bannerAd = new AdRequest.Builder().build();
            holder.adView.loadAd(bannerAd);
        }   else if (holder.view_type == APP_INSTALL) {
            holder.appName.setText("Student Revise");
            holder.appName.setTypeface(typeface);
            holder.appName.setTextColor(Color.BLACK);

            Glide.with(context).load(R.drawable.logo).into(holder.appIcon);

            holder.installApp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                    try {
                        context.startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        context.startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
                    }
                }
            });
        }
    }

    private String getPublishAtDate(String publishedAt) {
        String[] arrayPubliished = publishedAt.split("-");
        return arrayPubliished[2].substring(0, 2) + " " + getMonth(arrayPubliished[1]) + " " + arrayPubliished[0];
    }

    private String getMonth(String month) {
        if (month.equals("01")) {
            return "January";
        }
        if (month.equals("02")) {
            return "February";
        }
        if (month.equals("03")) {
            return "March";
        }
        if (month.equals("04")) {
            return "April";
        }
        if (month.equals("05")) {
            return "May";
        }
        if (month.equals("06")) {
            return "June";
        }
        if (month.equals("07")) {
            return "July";
        }
        if (month.equals("08")) {
            return "August";
        }
        if (month.equals("09")) {
            return "September";
        }

        if (month.equals("10")) {
            return "October";
        }

        if (month.equals("11")) {
            return "November";
        }

        if (month.equals("12")) {
            return "December";
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == HEAD) {
            Log.d(TAG, "getItemViewType: " + HEAD);
            return HEAD;
        }
        else if (position == Integer.parseInt(context.getString(R.string.app_install))) {
            Log.d(TAG, "getItemViewType: " + APP_INSTALL);
            return APP_INSTALL;
        }
        else if (youtubeModelList.get(position) == null) {
            Log.d(TAG, "getItemViewType: " + AD);
            return AD;
        }
        Log.d(TAG, "getItemViewType: " + LIST);
        return LIST;
    }

    @Override
    public int getItemCount() {
        return youtubeModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        int view_type;

        TextView listTitle;
        TextView publishedAt;
        TextView description;
        CircleImageView listThumb;
        LinearLayout listLinearLayout;
        ImageView favourite;

        TextView headTitle;
        //TextView headDescription;
        ImageView headThumb;
        RelativeLayout headLayout;

        AdView adView;


        TextView appName;
        ImageView appIcon;
        Button installApp;

        public ViewHolder(View itemView, int viewType) {
           super(itemView);
            if (viewType == LIST) {
                Log.d(TAG, "ViewHolder: " + LIST);
                listTitle = (TextView) itemView.findViewById(R.id.youtube_list_title);
                favourite = (ImageView) itemView.findViewById(R.id.favourite);
                description = (TextView) itemView.findViewById(R.id.youtube_list_description);
                publishedAt = (TextView) itemView.findViewById(R.id.youtube_list_published_at);
                listThumb = (CircleImageView) itemView.findViewById(R.id.youtube_list_thumbnail);
                listLinearLayout = (LinearLayout) itemView.findViewById(R.id.youtube_list_linear_layout);
                listLinearLayout.setOnClickListener(this);
                view_type = 5;
            } else if (viewType == HEAD) {
                Log.d(TAG, "ViewHolder: " + HEAD);
                headTitle = (TextView) itemView.findViewById(R.id.youtube_head_title);
                headThumb = (ImageView) itemView.findViewById(R.id.youtube_head_thumb);
                headLayout = (RelativeLayout) itemView.findViewById(R.id.youtube_head_relative_layout);
                headLayout.setOnClickListener(this);
            } else if (viewType == AD) {
                Log.d(TAG, "ViewHolder: " + AD);
                adView = (AdView) itemView.findViewById(R.id.adview_ad);
                view_type = 2;
            } else if (viewType == APP_INSTALL) {
                appName = (TextView) itemView.findViewById(R.id.app_name);
                appIcon = (ImageView) itemView.findViewById(R.id.app_icon);
                installApp = (Button) itemView.findViewById(R.id.install_app);
                view_type = 3;
            }

        }

        @Override
        public void onClick(View v) {
            YoutubeModel youtubeModel = youtubeModelList.get(getAdapterPosition());
            controller.onItemClick(youtubeModel.getVideoID());
        }
    }
}
