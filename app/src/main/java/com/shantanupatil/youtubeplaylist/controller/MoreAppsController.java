package com.shantanupatil.youtubeplaylist.controller;

import com.shantanupatil.youtubeplaylist.interfaces.IPassDataMoreApps;

public class MoreAppsController {
    IPassDataMoreApps passDataMoreApps;

    public MoreAppsController(IPassDataMoreApps passDataMoreApps) {
        this.passDataMoreApps = passDataMoreApps;
    }

    public void onItemClick(String url) {
        passDataMoreApps.passData(url);
    }
}
